import paho.mqtt.client as mqtt
import time
import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008

mcp = Adafruit_MCP3008.MCP3008(clk=18, cs=25, miso=23, mosi=24)
client = mqtt.Client()

def log_temperature():
	millivolts = mcp.read_adc(0) * (3300 / 1024)
	temp = ((millivolts - 100) / 10) - 40

	client.publish("room/temp", temp, 0, True)


if __name__ == "__main__":
	client.connect("192.168.0.51")

	while True:
		log_temperature()
		time.sleep(60)



