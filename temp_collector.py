import paho.mqtt.client as mqtt
from influxdb import InfluxDBClient

client = mqtt.Client()
database = InfluxDBClient("localhost", database="data")

def on_message(client, userdata, message):
    record_temp(float(message.payload))


def on_connect(client, userdata, flags, rc):
    client.subscribe("room/temp")

def record_temp(temp):
    database.create_database("data")
    database.write_points([{
        "measurement": "temp",
        "fields": {
            "value": temp
        }
    }])

if __name__ == "__main__":
    client.connect("192.168.0.51")
    client.on_connect = on_connect
    client.on_message = on_message
    client.loop_forever()
