import RPi.GPIO as GPIO
import paho.mqtt.client as mqtt
import threading
import time

GPIO.setmode(GPIO.BOARD)
GPIO.setup(36, GPIO.OUT)
GPIO.setup(38, GPIO.OUT)
GPIO.setup(40, GPIO.OUT)

green = GPIO.PWM(36, 120)
red = GPIO.PWM(38, 120)
blue = GPIO.PWM(40, 120)

def interpolate(value, target):
    delta = (target - value) / 10
    if abs(delta) < 0.1:
        return target
    return value + delta

class Light:
    def __init__(self):
        self.colour = [0, 0, 0]
        self.target_colour = [100, 50, 0]
        self.intensity = 1.0
        self.update_speed = 5

    def set_colour(self, r, g, b):
        self.target_colour[0] = float(r)
        self.target_colour[1] = float(g)
        self.target_colour[2] = float(b)

    def set_intensity(self, intensity):
        self.intensity = intensity / 100.0

    def update_light_forever(self):
        while True:
            self.update_light()
            time.sleep(0.1)

    def update_light(self):
        if self.colour == self.target_colour:
            return

        self.colour[0] = interpolate(self.colour[0], self.target_colour[0])
        self.colour[1] = interpolate(self.colour[1], self.target_colour[1])
        self.colour[2] = interpolate(self.colour[2], self.target_colour[2])

        red.ChangeDutyCycle(self.colour[0] * self.intensity)
        green.ChangeDutyCycle(self.colour[1] * self.intensity)
        blue.ChangeDutyCycle(self.colour[2] * self.intensity)

light = Light()

def temp_to_rgb(temp: float):
    xy = colour.temperature.cct

def on_message(client, userdata, msg):
    payload = msg.payload.decode("utf-8")
    print("payload", payload)

    if msg.topic == "room/light/colour":
        colour = [float(c)/255.0 * 100.0 for c in payload[4:-1].split(",")]
        light.set_colour(colour[0], colour[1], colour[2])
    elif msg.topic == "room/light/intensity":
        light.set_intensity(float(payload))
    elif msg.topic == "room/light/temperature":
        print(payload)
        r, g, b = temp_to_rgb(float(payload))


if __name__ == "__main__":
    print("Starting")
    try:
        update_thread = threading.Thread(target=light.update_light_forever)
        update_thread.start()
        green.start(0)
        red.start(0)
        blue.start(0)

        client = mqtt.Client()
        client.connect("192.168.0.51")
        client.subscribe("room/light/colour")
        client.subscribe("room/light/intensity")
        client.subscribe("room/light/temperature")

        client.on_message = on_message
        client.loop_forever()
    except KeyboardInterrupt:
        GPIO.cleanup()


